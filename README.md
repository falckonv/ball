### BALL

Reproduction of the Unity ball-game.

### CHALLENGES

The materials are not colliding correctly.

* Removed trigger on BoxCollider from all walls: then the ball is not escaping
anymore, but stick to the wall, not bouncing.
* Setting on Player (the balls Rigidbody) Collision Detection: set to Continous or Dynamic Continous --> after a while the ball takes off into space!(Notice: gravity is off, velocity in y-direction is 0
* Ball still sticks to the wall
* Looking at the game from low angle: the ball is slightly levitating after hitting the wall. With increased speed the ball levitate more in the y-direction. Eventually it goes over the wall. The player script is only steering it in the x and z directions, so no correction is possible through the game controllers as the ball bounces off its original y-position.
* Collider on Player (ball) was set to box-collider. Changing to sphere collider prevents the ball from taking off the ground. But still it stics to the wall, no real collision effect.
* With speed = 50 it is still possible to bounce the ball in the walls so that the ball levitates from the gound and go over the walls.

### SETTINGS
* Using Unity2018.4.8f1
* OSX

'''
Code here...
'''
