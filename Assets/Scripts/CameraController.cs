﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Follow camera - should use late update (after all items are updated!)
public class CameraController : MonoBehaviour
{
	public GameObject player;

	private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
		offset = transform.position - player.transform.position;
    }

    // LateUpdate to make sure all items are updated before changing position
    void LateUpdate()
    {
		transform.position = player.transform.position + offset;
    }
}
